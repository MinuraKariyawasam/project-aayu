package com.mindscape.aayu;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

public class AppQuiz extends AppCompatActivity {

    //declaring elements
    //quizNumber
    private TextView cntLbl;
    //questions
    private TextView quizLbl;
    //answer buttons
    private Button btnAnswer1;
    private Button btnAnswer2;
    private Button btnAnswer3;
    private Button btnAnswer4;
    static final private int questionCount = 10;

    private String correctAnswer;
    private int correctAnswerCount = 0;
    private int countQuestion = 1;
    private int quizScore = 0;

    //initializing arrayList
    ArrayList<ArrayList<String>> questionArray = new ArrayList<>();

    String dataForQuiz[][] = {
        //{"Question",rightAnswer","answer1","answer2","answer3"}
            {"What is the scientific name of Jew Plum which is locally known as Ambarella ?","Spondias dulcis","Abroma augusta","Uraria picta","Nypa fruiticans"},
            {"Which plant is known to be the Mother of Medicine ?","Holy basil","Basil","Arjun","Jatropa"},
            {"Out of below, Which part of Aralu plant is used for treatments ?","Pericap of fruit","Barks","Roots","Flowers"},
            {"For which type of treatments, Asamodagam is not used for ?","Headache","Asthma","Hiccough","Dysentery"},
            {"Which parts of the plant Gas Nivithi (local name) is used for treatments ?","Roots","Flowers","Stem","Fruit"},
            {"What is the local name of Zingiber officinale ? ","Inguru","Kaha","Koththamalli","Helamba"},
            {"What is the edible part in the plant, Welmee ?","Root","Stem","Fruit","Flowers"},
            {"Which is not a treatment type where Sesame seeds are not used for?","Headache","Burns","Wounds","Cystitis"},
            {"Which is a edible plant in the china rose plant ?","Flowers","Stem","Bark","Roots"},
            {"What is the local name of Munronia pinnata ?","Bin kohomba","Belipatta","Akkapana","Bimpol"},
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_app_quiz);

        cntLbl = (TextView)findViewById(R.id.cntLbl);
        quizLbl = (TextView)findViewById(R.id.resultLbl);
        btnAnswer1 = (Button)findViewById(R.id.anwBtn1);
        btnAnswer2 = (Button)findViewById(R.id.anwBtn2);
        btnAnswer3 = (Button)findViewById(R.id.anwBtn3);
        btnAnswer4 = (Button)findViewById(R.id.anwBtn4);

        //accessing quizArray from quizdata
        for(int i = 0; i < dataForQuiz.length; i++){

            //arrange array
            ArrayList<String> tempryArry = new ArrayList<>();
            tempryArry.add(dataForQuiz[i][0]); //question
            tempryArry.add(dataForQuiz[i][1]); // correct answer
            tempryArry.add(dataForQuiz[i][2]); //answer1
            tempryArry.add(dataForQuiz[i][3]); //answer2
            tempryArry.add(dataForQuiz[i][4]); //answer3

            //merge temporary array to array with question array
            questionArray.add(tempryArry);
        }
        loadTheNextQuestion();
    }

    public void loadTheNextQuestion(){

        //adding up the question view
         cntLbl.setText("Question " + countQuestion);

         //randomizing for the size of array
        Random random  = new Random();
        int numberRandom = random.nextInt(questionArray.size());

        //accessing randomize question
        ArrayList<String> question = questionArray.get(numberRandom);

        //setting up the question to the view (//{"rightAnswer","answer1","answer2","answer3"})
        quizLbl.setText(question.get(0));
        correctAnswer = question.get(1);

        //randomizing answers, without questions
        question.remove(0);
        Collections.shuffle(question);

        //setting up answers
        btnAnswer1.setText(question.get(0));
        btnAnswer2.setText(question.get(1));
        btnAnswer3.setText(question.get(2));
        btnAnswer4.setText(question.get(3));

        //unaccessing from main array
        questionArray.remove(numberRandom);
    }

    public void correctAnswerCheck(View v){
        //accessing user input button
        Button clickedAnswerBtn = (Button) findViewById(v.getId());
        String textClickedBtn = clickedAnswerBtn.getText().toString();

        String alertEffect;
         if (textClickedBtn.equals(correctAnswer)){
             alertEffect = "Correct";
             correctAnswerCount++;
             //quizScore++;
             //countQuestion++;
         } else {
             alertEffect = "Wrong";
             //countQuestion++;
         }

         //alert box
        AlertDialog.Builder buildAlertDialog = new AlertDialog.Builder(this);
         buildAlertDialog.setTitle(alertEffect);
         buildAlertDialog.setMessage("Correct Answer : " + correctAnswer);
         buildAlertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
             @Override
             public void onClick(DialogInterface dialog, int which) {
                 if (countQuestion == questionCount){
                     Intent intentQuiz = new Intent(getApplicationContext(),QuizScore.class);
                     intentQuiz.putExtra("CORRECT_ANSWS", correctAnswerCount);
                     startActivity(intentQuiz);
                 }else{
                     countQuestion++;
                     loadTheNextQuestion();
                 }
             }
         });
         buildAlertDialog.setCancelable(false);
         buildAlertDialog.show();
    }
}